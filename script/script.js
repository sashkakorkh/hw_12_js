/*Exercise 1*/
/*Для роботи з інпутами недостатньо відслідковувати події тільки клавіатури,
бо є ще інші способи вводу інформації, такі як вставити за допомогою миші та контекстного меню та мікрофон*/

/*Exercise 2*/
const keys = document.querySelectorAll("[data-key]");

function showKey () {
        document.body.addEventListener('keydown', e => {
            keys.forEach(item=> {
                console.log(e.key)
                let key = item.dataset.key.toLowerCase()
            if( key !== e.key.toLowerCase()) {
                item.classList.remove('blue-color')
                return;
            }
                item.classList.add('blue-color');
        })
    })
}

showKey()